package visao;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class TelaGerenciarPaciente extends JPanel {
	
	private JButton btnCadastrarPaciente;
	private JButton btnLimparTela;
	
	private JTextField fieldCPF;
	private JTextField fieldNome;
	private JTextField fieldDataNasc;
	private JTextField fieldEndereco;
	private JTextField fieldNomePai;
	private JTextField fieldNomeMae;
	
	private JComboBox<String> comboBoxTipoSanguineo;
	

	/**
	 * Create the panel.
	 */
	public TelaGerenciarPaciente() {
		
		setBounds(0, 0, 650, 500);
		setLayout(new MigLayout("", "[grow][][grow][grow]", "[40.00][][40.00][][40.00][][40.00][][40.00][][][40.00]"));
		
		JLabel lblTitulo = new JLabel("Gerenciar Pacientes");
		lblTitulo.setFont(new Font("Verdana", Font.BOLD, 20));
		add(lblTitulo, "cell 0 0");
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblCPF, "flowx,cell 0 2");
		
		fieldCPF = new JTextField();
		fieldCPF.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldCPF, "cell 0 2,growx");
		fieldCPF.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblNome, "flowx,cell 1 2 3 1");
		
		JLabel lblDataNasc = new JLabel("Data de Nascimento");
		lblDataNasc.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblDataNasc, "flowx,cell 0 4, spanx 4");
		
		fieldDataNasc = new JTextField();
		fieldDataNasc.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldDataNasc, "cell 0 4 4 1");
		fieldDataNasc.setColumns(10);
		
		JLabel lblNomePai = new JLabel("Nome do Pai");
		lblNomePai.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblNomePai, "flowx,cell 0 6");
		
		fieldNomePai = new JTextField();
		fieldNomePai.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldNomePai, "cell 0 6,growx");
		fieldNomePai.setColumns(10);
		
		JLabel lblNomeMae = new JLabel("Nome da M\u00E3e");
		lblNomeMae.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblNomeMae, "cell 1 6");
		
		fieldNomeMae = new JTextField();
		fieldNomeMae.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldNomeMae, "cell 2 6 2 1,growx");
		fieldNomeMae.setColumns(10);
		
		JLabel lblTipoSanguineo = new JLabel("Tipo Sanguineo");
		lblTipoSanguineo.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblTipoSanguineo, "flowx,cell 0 8");
		
		btnCadastrarPaciente = new JButton("Cadastrar");
		btnCadastrarPaciente.setFont(new Font("Verdana", Font.BOLD, 14));
		btnCadastrarPaciente.setActionCommand("cadastrar-paciente"); // Alterando o action command do botao
		add(btnCadastrarPaciente, "flowx,cell 0 11");
		
		btnLimparTela = new JButton("Limpar Tela");
		btnLimparTela.setFont(new Font("Verdana", Font.BOLD, 14));
		btnLimparTela.setActionCommand("limpar-tela"); // Alterando o action command do botao
		add(btnLimparTela, "cell 0 11");
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblEndereco, "cell 0 4 4 1");
		
		fieldEndereco = new JTextField();
		fieldEndereco.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldEndereco, "cell 0 4 4 1,growx");
		fieldEndereco.setColumns(10);
		
		comboBoxTipoSanguineo = new JComboBox<>();
		comboBoxTipoSanguineo.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(comboBoxTipoSanguineo, "cell 0 8,growx");
		
		// Adicionando os tipos sanguineos no comboBox
		comboBoxTipoSanguineo.addItem("A+");
		comboBoxTipoSanguineo.addItem("A-");
		comboBoxTipoSanguineo.addItem("B+");
		comboBoxTipoSanguineo.addItem("B-");
		comboBoxTipoSanguineo.addItem("O+");
		comboBoxTipoSanguineo.addItem("O-");
		comboBoxTipoSanguineo.addItem("AB+");
		comboBoxTipoSanguineo.addItem("AB-");
		
		fieldNome = new JTextField();
		fieldNome.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(fieldNome, "cell 1 2 2 1,growx");
		fieldNome.setColumns(10);

	}

	public JTextField getFieldCPF() {
		return fieldCPF;
	}

	public void setFieldCPF(JTextField fieldCPF) {
		this.fieldCPF = fieldCPF;
	}

	public JTextField getTextField() {
		return fieldNome;
	}

	public void setTextField(JTextField textField) {
		this.fieldNome = textField;
	}

	public JButton getBtnCadastrarPaciente() {
		return btnCadastrarPaciente;
	}

	public void setBtnCadastrarPaciente(JButton btnCadastrarPaciente) {
		this.btnCadastrarPaciente = btnCadastrarPaciente;
	}

	public JButton getBtnLimparTela() {
		return btnLimparTela;
	}

	public void setBtnLimparTela(JButton btnLimparTela) {
		this.btnLimparTela = btnLimparTela;
	}

	public JTextField getFieldNome() {
		return fieldNome;
	}

	public void setFieldNome(JTextField fieldNome) {
		this.fieldNome = fieldNome;
	}

	public JTextField getFieldDataNasc() {
		return fieldDataNasc;
	}

	public void setFieldDataNasc(JTextField fieldDataNasc) {
		this.fieldDataNasc = fieldDataNasc;
	}

	public JTextField getFieldEndereco() {
		return fieldEndereco;
	}

	public void setFieldEndereco(JTextField fieldEndereco) {
		this.fieldEndereco = fieldEndereco;
	}

	public JTextField getFieldNomePai() {
		return fieldNomePai;
	}

	public void setFieldNomePai(JTextField fieldNomePai) {
		this.fieldNomePai = fieldNomePai;
	}

	public JTextField getFieldNomeMae() {
		return fieldNomeMae;
	}

	public void setFieldNomeMae(JTextField fieldNomeMae) {
		this.fieldNomeMae = fieldNomeMae;
	}

	public JComboBox<String> getComboBoxTipoSanguineo() {
		return comboBoxTipoSanguineo;
	}

	public void setComboBoxTipoSanguineo(JComboBox<String> comboBoxTipoSanguineo) {
		this.comboBoxTipoSanguineo = comboBoxTipoSanguineo;
	}

}
