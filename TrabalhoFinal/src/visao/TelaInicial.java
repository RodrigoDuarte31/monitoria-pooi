package visao;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class TelaInicial extends JPanel {

	/**
	 * Create the panel.
	 */
	public TelaInicial() {
		
		setBounds(0, 0, 650, 500);
		setLayout(new BorderLayout(0, 0));
		
		JLabel lblImagem = new JLabel("");
		lblImagem.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblImagem, BorderLayout.CENTER);
		
		lblImagem.setBounds(0, 0, 128, 128);
		
		ImageIcon imagem = new ImageIcon("/figuras/hospital-icon.png");
		try {
			lblImagem.setIcon(new ImageIcon(ImageIO.read(TelaInicial.class.getResource("/figuras/hospital-icon.png"))
					.getScaledInstance(lblImagem.getWidth(), lblImagem.getHeight(), BufferedImage.TYPE_INT_RGB)));
		} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
		}

	}

}
