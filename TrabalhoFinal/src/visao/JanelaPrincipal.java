package visao;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JanelaPrincipal extends JFrame {

	private JPanel contentPane;
	private CardLayout card;
	private JPanel telaInicial;
	
	private JMenuItem itemGerenciarPaciente;

	public JanelaPrincipal() {
		setTitle("Sistema de Atendimento Hospitalar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		
		setResizable(false); // Desabilitando o redimensionamento de tela
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menuPaciente = new JMenu("Paciente");
		menuPaciente.setFont(new Font("Verdana", Font.PLAIN, 16));
		menuBar.add(menuPaciente);
		
		itemGerenciarPaciente = new JMenuItem("Gerenciar Paciente");
		itemGerenciarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				card.show(contentPane, "painel2"); // Fazendo a transicao para a tela de Gerenciar Paciente (ver classe controle)
				
			}
		});
		itemGerenciarPaciente.setFont(new Font("Verdana", Font.PLAIN, 14));
		itemGerenciarPaciente.setActionCommand("gerenciar-paciente");
		menuPaciente.add(itemGerenciarPaciente);
		
		JMenu menuAtendimento = new JMenu("Atendimento");
		menuAtendimento.setFont(new Font("Verdana", Font.PLAIN, 16));
		menuBar.add(menuAtendimento);
		
		JMenuItem itemGerarAtendimento = new JMenuItem("Gerar Atendimento");
		itemGerarAtendimento.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuAtendimento.add(itemGerarAtendimento);
		
		JMenu menuConsulta = new JMenu("Consulta");
		menuConsulta.setFont(new Font("Verdana", Font.PLAIN, 16));
		menuBar.add(menuConsulta);
		
		JMenuItem itemGerarConsulta = new JMenuItem("Gerar Consulta");
		itemGerarConsulta.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuConsulta.add(itemGerarConsulta);
		
		JMenuItem itemEncaminharInternacao = new JMenuItem("Encaminhar para Interna\u00E7\u00E3o");
		itemEncaminharInternacao.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuConsulta.add(itemEncaminharInternacao);
		
		JMenuItem itemFinalizarAtendimento = new JMenuItem("Finalizar Atendimento");
		itemFinalizarAtendimento.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuConsulta.add(itemFinalizarAtendimento);
		
		JMenu menuRelatorios = new JMenu("Relat\u00F3rios");
		menuRelatorios.setFont(new Font("Verdana", Font.PLAIN, 16));
		menuBar.add(menuRelatorios);
		
		JMenuItem itemFilaLeitos = new JMenuItem("Listar pacientes em espera de leito");
		itemFilaLeitos.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuRelatorios.add(itemFilaLeitos);
		
		JMenuItem itemListarPacientesInternados = new JMenuItem("Listar pacientes internados");
		itemListarPacientesInternados.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuRelatorios.add(itemListarPacientesInternados);
		
		JMenuItem itemListarLeitosVagos = new JMenuItem("Listar leitos vagos");
		itemListarLeitosVagos.setFont(new Font("Verdana", Font.PLAIN, 14));
		menuRelatorios.add(itemListarLeitosVagos);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		card = new CardLayout(); // Iniciando o CardLayout
		
		contentPane.setLayout(card); // Configurando o CardLayout como gerenciador de layout do contentPane
		
		telaInicial = new TelaInicial(); // Iniciando JPanel da tela inicial
		
		contentPane.add(telaInicial, "painel1"); // Adicionando a tela inicial ao contentPane
		
	}

	public JMenuItem getItemGerenciarPaciente() {
		return itemGerenciarPaciente;
	}

	public void setItemGerenciarPaciente(JMenuItem itemGerenciarPaciente) {
		this.itemGerenciarPaciente = itemGerenciarPaciente;
	}

}
